class ParamConfigsController < ApplicationController
  before_action :set_param_config, only: [:show, :update, :destroy]

  # GET /param_configs
  def index
    @param_configs = ParamConfig.all

    render json: @param_configs
  end

  # GET /param_configs/1
  def show
    render json: @param_config
  end

  # POST /param_configs
  def create
    @param_config = ParamConfig.new(param_config_params)

    if @param_config.save
      render json: @param_config, status: :created, location: @param_config
    else
      render json: @param_config.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /param_configs/1
  def update
    if @param_config.update(param_config_params)
      render json: @param_config
    else
      render json: @param_config.errors, status: :unprocessable_entity
    end
  end

  # DELETE /param_configs/1
  def destroy
    @param_config.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_param_config
      @param_config = ParamConfig.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def param_config_params
      params.require(:param_config).permit(:namespace, :value)
    end
end
