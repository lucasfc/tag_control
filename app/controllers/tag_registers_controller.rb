class TagRegistersController < ApplicationController
  before_action :set_tag_register, only: [:show, :update, :destroy]

  # GET /tag_registers
  def index
    @tag_registers = TagRegister.all

    render json: @tag_registers
  end

  # GET /tag_registers/1
  def show
    render json: @tag_register
  end

  # POST /tag_registers
  def create
    @tag_register = TagRegister.new(tag_register_params)

    if @tag_register.save
      render json: @tag_register, status: :created, location: @tag_register
    else
      render json: @tag_register.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /tag_registers/1
  def update
    if @tag_register.update(tag_register_params)
      render json: @tag_register
    else
      render json: @tag_register.errors, status: :unprocessable_entity
    end
  end

  # DELETE /tag_registers/1
  def destroy
    @tag_register.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_tag_register
      @tag_register = TagRegister.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def tag_register_params
      params.require(:tag_register).permit(:person_tag_id, :device_reader_id, :area_id, :exit_at)
    end
end
