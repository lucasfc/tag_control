class PersonTagsController < ApplicationController
  before_action :set_person_tag, only: [:show, :update, :destroy]

  # GET /person_tags
  def index
    @person_tags = PersonTag.all

    render json: @person_tags
  end

  # GET /person_tags/1
  def show
    render json: @person_tag
  end

  # POST /person_tags
  def create
    @person_tag = PersonTag.new(person_tag_params)

    if @person_tag.save
      render json: @person_tag, status: :created, location: @person_tag
    else
      render json: @person_tag.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /person_tags/1
  def update
    if @person_tag.update(person_tag_params)
      render json: @person_tag
    else
      render json: @person_tag.errors, status: :unprocessable_entity
    end
  end

  # DELETE /person_tags/1
  def destroy
    @person_tag.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_person_tag
      @person_tag = PersonTag.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def person_tag_params
      params.require(:person_tag).permit(:person_id, :device_tag_id, :inactivated_at, :active)
    end
end
