class Reply < ApplicationRecord
  belongs_to :question
  has_many :reply_people

  def self.counter(code)
    reply = Reply.find_by_code(code)
    if reply.blank?
      {status: false, mensagem: "Resposta não encontrada"}
    end
    {status: true, conter: reply.reply_people.count}
  end
  
end
