require 'test_helper'

class TagRegistersControllerTest < ActionDispatch::IntegrationTest
  setup do
    @tag_register = tag_registers(:one)
  end

  test "should get index" do
    get tag_registers_url, as: :json
    assert_response :success
  end

  test "should create tag_register" do
    assert_difference('TagRegister.count') do
      post tag_registers_url, params: { tag_register: { area_id: @tag_register.area_id, device_reader_id: @tag_register.device_reader_id, exit_at: @tag_register.exit_at, person_tag_id: @tag_register.person_tag_id } }, as: :json
    end

    assert_response 201
  end

  test "should show tag_register" do
    get tag_register_url(@tag_register), as: :json
    assert_response :success
  end

  test "should update tag_register" do
    patch tag_register_url(@tag_register), params: { tag_register: { area_id: @tag_register.area_id, device_reader_id: @tag_register.device_reader_id, exit_at: @tag_register.exit_at, person_tag_id: @tag_register.person_tag_id } }, as: :json
    assert_response 200
  end

  test "should destroy tag_register" do
    assert_difference('TagRegister.count', -1) do
      delete tag_register_url(@tag_register), as: :json
    end

    assert_response 204
  end
end
