class AddCodeToArea < ActiveRecord::Migration[5.1]
  def change
    add_column :areas, :code, :string
  end
end
