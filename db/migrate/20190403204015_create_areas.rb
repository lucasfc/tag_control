class CreateAreas < ActiveRecord::Migration[5.1]
  def change
    create_table :areas do |t|
      t.string :name
      t.string :description
      t.integer :limit
      t.boolean :exit_register
      t.text :webhook

      t.timestamps
    end
  end
end
